from pyo import *
from NeuroPy import NeuroPy
import time

IP_BELA = '10.0.0.2'
IP_GUI = '192.168.2.179'

s = Server(audio='jack')
s.setJackAuto(False, False)
s.boot()

bw=NeuroPy("/dev/rfcomm0")
bw.start()

a = SigTo([bw.attention, bw.meditation, bw.poorSignal])

b = OscSend(a, port=10001, address=['/attention', '/meditation', '/poorSignal'], host=IP_BELA)
b2 = OscSend(a, port=10001, address=['/attention', '/meditation', '/poorSignal'], host=IP_GUI)

#b2 = OscSend(a, port=10001, address=['/attention', '/meditation', '/rawValue', '/delta', '/theta', '/lowAlpha', '/highAlpha', '/lowBeta', '/highBeta', '/lowGamma', '/midGamma', '/poorSignal', '/blinkStrength'], host='192.168.2.179')

s.start()

while True:
    #print [bw.attention, bw.meditation, bw.rawValue, bw.delta, bw.theta, bw.lowAlpha, bw.highAlpha, bw.lowBeta, bw.highBeta, bw.lowGamma, bw.midGamma, bw.poorSignal, bw.blinkStrength]
    print([bw.attention, bw.meditation, bw.poorSignal])
    a.setValue([bw.attention, bw.meditation, bw.poorSignal])
    time.sleep(0.01)
