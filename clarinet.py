import random
from pyo import *

# MIDI pedals messages for disklavier
# Sustain Pedal (Controller 64)
# Una Corda Pedal (Controller 67)
# Sostenuto Pedal (Controller 66)

# Set server
pm_list_devices()
s = Server(audio='jack')

# Open MIDI output device.
s.setMidiOutputDevice(99)

# Then boot the Server.
s.boot()

# OSC setup
osc = OscReceive(port=10001, address=['/attention', '/meditation', '/poorSignal'])

# Generates an audio ramp from 21 to 108, from
# which MIDI pitches will be extracted.
pitch = Phasor(freq=11, mul=87, add=21)

######## MIDI CUE 1 #################

# Global variable to count the down and up beats.
count = 0

def midi_event_cue1():
    meditation = int(osc.get(identifier='/meditation', all=False))
    global count
    # Retrieve the value of the pitch audio stream and convert it to an int.
    pit = int(pitch.get())
    #WOW usar isso aqui, pequenos movimentos das teclas
    # aumentando DUR aos poucos, saindo notas aos poucos, jogo de cles no basson ou clarinette
    # se transformando em notas longas bem devagar, improvisacao guiada pelo computador
    # alguem brincar com o pedal, muda tudo
    #dur = random.randint(10, 100)
    dur = meditation
    #vel = random.randint(10, 10)
    vel = meditation

    # Increase and wrap the count to generate a 4 beats sequence.
    count = (count + 1) % 4

    #print("pitch: %d, velocity: %d, duration: %d" % (pit, vel, dur))

    # The Server's `makenote` method generates a noteon event immediately
    # and the correponding noteoff event after `duration` milliseconds.
    s.makenote(pitch=pit, velocity=vel, duration=dur)

# Generates a MIDI event every X milliseconds.
pat_cue1 = Pattern(midi_event_cue1, 0.05).play()


########### MIDI CUE 2 ###################

# Counter back to 0
count = 0
def midi_event_cue2():
    global count
    # Retrieve the value of the pitch audio stream and convert it to an int.
    pit = int(pitch.get())
    #pitch.freq=50
    #pitch.add=30
    #pat.setTime(0.2)

    # If the count is 0 (down beat), play a louder and longer event, otherwise
    # play a softer and shorter one.
    if count == 0:
        vel = random.randint(80, 100)
        dur = random.randint(200,400)
    else:
        vel = random.randint(50, 75)
        dur = random.randint(70,125)

    # Increase and wrap the count to generate a 4 beats sequence.
    count = (count + 1) % 4

    print("pitch: %d, velocity: %d, duration: %d" % (pit, vel, dur))

    # The Server's `makenote` method generates a noteon event immediately
    # and the correponding noteoff event after `duration` milliseconds.
    s.makenote(pitch=pit, velocity=vel, duration=dur)
    s.makenote(pitch=pit+random.randint(1,5), velocity=vel, duration=dur)

    # pitch.add = 0, then
    # pat.setTime(0.05)
    # mic contact under the piano, feeding delay over time
    # then stop midi and keep just mechanic noise + delay
    # this will generate a nice feedback: play avec!
    # nice octaves C# and D#
    # turn delay feedack down to 0 little by little
    # cello harmonics start playing and mix with the piano

######## Global score for the piece ###################

# Generates a MIDI event every X milliseconds.
pat_cue2 = Pattern(midi_event_cue2, 0.5).stop()

# Starts with Cue 1
cue = 1

# Minimum duration of a cue - this avoids passing over many cues at once
min_cue_time = 0


# Audio files
homosapiens = SfPlayer('444166__cloe-king__wine-glass-ring.wav').play()

# Global score
def score():
    global cue, min_cue_time, pat_cue1, homosapiens, homosapiens_midi

    meditation = int(osc.get(identifier='/meditation', all=False))
    attention = int(osc.get(identifier='/attention', all=False))

    # Alter pattern time based on OSC input
    #pat_cue1.setTime(Scale(osc['/meditation'], 0, 100, 0.5, 0.05))
    print("MEDITATION:", meditation)
    print("ATTENTION:", attention)

    # Increments cue duration, in seconds
    min_cue_time = min_cue_time + 1

    if cue == 1:
        print('CUE:', cue)
        # isabelle
        # if attention > 80 or meditation > 80:
        if meditation > 95:
            cue = 2
            pat_cue1.stop()
            s.ctlout(64, 127)
            s.makenote(pitch=40, velocity=100, duration=100)
            s.ctlout(64, 0, 10000) # stop sustain pedal after 10 seconds
            min_cue_time = 0

    if cue == 2:
        print('CUE:', cue)
        #pat_cue2.play()
        homosapiens.out()
        hz = Yin(Input())
        mid = FToM(hz)
        print(mid.get())

        #fr = Snap(homosapiens_midi, choice=[0,2,5,7,9], scale=1)
        #s.makenote(pitch=pit, velocity=100, duration=100)

    if cue == 3:
        print('CUE:', cue)

    if cue == 4:
        print('CUE:', cue)

    if cue == 5:
        print('CUE:', cue)


# for cue 1 to 2 = 3sec, meditation > 95

mainTime = Metro(time=4).play()
mainFunc = TrigFunc(mainTime, score)

s.gui(locals())
