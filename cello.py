from pyo import *
import random

s = Server(audio='jack')
s.setMidiOutputDevice(99)
s.boot()

class Freq_Listenner(object):

    def __init__(self):
        self.entreeMicroReal = Input(mul=1)
        self.entreeMicro = SfPlayer('bwv582.wav')
        #self.entreeMicro2 = SfPlayer('bwv582.wav')
        self.entreeMicro.setSpeed(Randi(0.5, 2, 0.1))
        self.entreeMicro.out()
        #self.freqFondamentale = Yin(self.entreeMicro, 0.2, 40, 11500, 9999, 1024, 1, 0)
        self.freqFondamentale = Yin(self.entreeMicroReal, 0.5, 20, 20000, 10000)
        self.freqFondamentaleOvertones = [ self.freqFondamentale*i for i in range(1,10) ]
        self.freqFondamentaleMidi = FToM(self.freqFondamentale)
        self.freqFondamentaleMidiOvertones = [ FToM(i) for i in self.freqFondamentaleOvertones ]

        self.MIDINOTE=0            #valeur midi de la hauteur de note chantee
        self.FREQFLAG=0            #=1 quand changement de note

        self.freqMidiRound=0
        self.freqCompteur=0

        # EEG CONTROL HERE
        self.pattern = Pattern(self.boucleFreq, Randi(0.05, 1, 7)).play()
        #self.pattern = Pattern(self.boucleFreq, Randi(0.05, 0.5, 1)).play()
        self.counter = 0

        print("freq_Listenner correctement initialise")

        #s.ctlout(64, 0) # stop sustain pedal
        s.ctlout(64, 127) # open sustain pedal
    def boucleFreq(self):
        if 1: # self.AMPFLAG==1:
            self.new_freqMidi = self.freqFondamentaleMidi.get()
            self.new_freqMidiRound = round(self.new_freqMidi)

            if (self.new_freqMidiRound==self.freqMidiRound) | (abs(self.new_freqMidi-self.MIDINOTE)<=0.6):
                self.freqCompteur += 1
            else:
                self.freqCompteur = 0

            #if self.freqCompteur == 10:
            if self.counter == 1000:
                s.ctlout(64, 127) # open sustain pedal
                #self.entreeMicro.setSpeed(1)
                #self.entreeMicro.out([0,1])
                self.MIDINOTE=int(round(self.new_freqMidiRound))
                self.FREQFLAG=1
            if self.counter == 2000:
#            if self.freqCompteur > 20:
                s.ctlout(64, 0) # stop sustain pedal
                self.entreeMicro.stop()
                self.entreeMicro2.out()


            self.freqMidiRound = self.new_freqMidiRound
            pitch = int(self.freqMidiRound)
            pitch_overtones = [ int(i.get()) for i in self.freqFondamentaleMidiOvertones ]
            s.makenote(pitch=pitch, velocity=random.randint(50, 800), duration=1)
            s.makenote(pitch=pitch-12, velocity=random.randint(30, 70), duration=1)
            s.makenote(pitch=pitch_overtones[random.randint(0,3)], velocity=random.randint(10,30), duration=1)
            #s.makenote(pitch=pitch_overtones[random.randint(0,8)], velocity=random.randint(10,30), duration=1)
            print('COUNTER', self.counter)
            print('MIDI', pitch)
            print('MIDI OVERTONES', pitch_overtones)
            self.counter = self.counter + 1

fl = Freq_Listenner()

s.gui(locals())
