from pyo import *
import random
import os
from instruments import *

s = Server(audio='jack')
s.setMidiOutputDevice(99)
s.boot()

m = MyFreezing()
m2 = MyFreezing()
m3 = MyFreezing()
m.stop()
m2.stop()
m3.stop()

def intro_pedal():
    s.ctlout(64, 127)

################# BEGIN  GESTURE 00 ##################

intro_speech = Speech(['intro.wav'])
sines = IntroSines()

def g00():
    global intro_speech
    intro_speech.play()

g00Time = Metro(time=Randi(31, 39)).stop()
g00Func = TrigFunc(g00Time, g00)

################# BEGIN  GESTURE 01 ##################

piano_flag = True

def g01():
    global piano_flag
    sines.play()
    intro_speech.stop()
    if piano_flag == True:
       s.makenote(pitch=22, velocity=random.randint(10, 25), duration=20000)
       s.makenote(pitch=79, velocity=random.randint(50, 70), duration=20000)
       s.makenote(pitch=91, velocity=random.randint(50, 70), duration=20000)
    m.pvb.setPitch(random.uniform(0.90, 1.1))
    m.refresh()
    # send midi note

g01Time = Metro(time=Randi(20, 35)).stop()
g01Func = TrigFunc(g01Time, g01)

################# BEGIN  GESTURE 02 ##################

high = HighFreq(mul=.05)

def g02():
    global high
    high.play()

g02Time = Metro(time=Randi(10, 30)).stop()
g02Func = TrigFunc(g02Time, g02)

################# BEGIN  GESTURE 03 ##################

snoise = SmoothNoise(mul=.25, dur=0.8)

def g03():
    global snoise
    snoise.play()

g03Time = Metro(time=Randi(10, 30)).stop()
g03Func = TrigFunc(g03Time, g03)

################# BEGIN  GESTURE 04 ##################

def g04():
    s.makenote(pitch=22, velocity=random.randint(40, 60), duration=20000)
    s.makenote(pitch=79, velocity=random.randint(40, 60), duration=20000)
    s.makenote(pitch=91, velocity=random.randint(20, 30), duration=20000)
    m.pvb.setPitch(random.uniform(0.9, 1.1))
    m2.pvb.setPitch(random.uniform(0.9, 1.1))
    m3.pvb.setPitch(random.uniform(0.9, 1.1))
    m.refresh()
    m2.refresh()
    m3.refresh()
    # send midi note

g04Time = Metro(time=Randi(20, 25)).stop()
g04Func = TrigFunc(g04Time, g04)

################# BEGIN GESTURE 05 ##################
# Stops high pitches
# Calls a new speech in repetition

g05_player_rev = None

def g05():
    global sines, g05_player_rev
    sines.stop()
    g05_player = SfPlayer(os.listdir('texts_speech'))
    g05_player_rev = Freeverb(g05_player, size=[.3,.2], damp=.5, bal=.3, mul=0.1).out()

g05Time = Metro(time=Randi(1, 5)).stop()
g05Func = TrigFunc(g05Time, g05)


################ SCORE ########################

time = -1

# Random speech to be called
speech_random = Speech(soundfile=os.listdir("44_statements"))
speech_random_time = Metro(time=Randi(25, 40)).stop()
speech_random_func = TrigFunc(speech_random_time, speech_random.play)

# GPT2 texts right before interlude
interlude_text = Speech(os.listdir('texts_speech'))
interlude_text_time = Metro(time=Randi(25, 40)).stop()
interlude_text_func = TrigFunc(interlude_text_time, interlude_text.play)

#def voice():
#    f = random.choice(os.listdir("44_statements"))
#    player = SfPlayer(f).play()
#    player_rev = Freeverb(player, size=[.3,.2], damp=.5, bal=.3, mul=.2).out()

def score():
    global time, m, m2, m3, interlude_text, piano_flag
    time += 1
    high.setDur(random.uniform(0.5, 1.5))
    snoise.setDur(random.uniform(0.5, 1.5))

    if time == 1:
        print(time)
        m.play() 
        g00Time.play()
    
    if time == 50:
        print(time)
        m2.play() 
        g01Time.play()

    if time == 80:
        print(time)
        g00Time.stop()

    if time == 120:
        print(time)
        piano_flag = False

    if time == 140:
        print(time)
        speech_random_time.play()

## Two minutes no piano only flute and voice

    if time == 240:
        print(time)
        g04Time.play() # starts new piano with low notes
        g01Time.stop() # stops initial piano
        m3.play() 
    
    if time == 300:
        print(time)
        g02Time.play()
        #m.stop()

    if time == 320:
        print(time)
        g04Time.stop() # stops all piano
        interlude_text_time.play()
        g03Time.play() 

    # no piano at all
    # 1 minute of instrumental duo + synth statements
    # call the serial (interlude) script
    if time == 370:
        print(time)
        m2.stop()

    # stop everything, repeated synth statement, keep a single freezing loop
    if time == 430:
        print(time)
        g01Time.stop()
        g02Time.stop()
        g03Time.stop()
        #g05Time.play()

mainTime = Metro(time=1).play()
mainFunc = TrigFunc(mainTime, score)
    

s.gui(locals())

