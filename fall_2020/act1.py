import random
from pyo import *
from instruments import Speech
import subprocess

pm_list_devices()

s = Server(audio='jack', duplex=0, nchnls=2)

# Open all MIDI output devices.
s.setMidiOutputDevice(99)

# Then boot the Server.
s.boot()

speech_intro = Speech(['intro.wav'], loop=0)
speech_intro.play()

# close pedal
s.ctlout(64, 0)

# set random-ish pattern time
pat_time = XnoiseDur(dist=11, min=15, max=20)
speech = Speech(['restart.wav'])

time_counter = 0
def time_events():
    global s, time_counter, pat_time, pat
    time_counter = time_counter + 1
    print(time_counter)
    print((pat_time.min, pat_time.max))
    
    d = random.choice([0,1])
    if d == 1:
        s.ctlout(64, 0)
    else:
        s.ctlout(64, 127)
       
    if time_counter == 10:
       pat_time.max = 10 
       pat_time.min =  5
  
    if time_counter == 20:
        pat_time.max = 5 
        pat_time.min = .5 

    if time_counter > 50 and pat_time.min > .1:
        pat_time.max = pat_time.min
        pat_time.min = pat_time.min - .05

    if time_counter == 50:
        s.ctlout(64, 127)

    if time_counter == 80:
        pat_time.max = 100
        pat_time.min = 100
        vel = 50
        dur = 2000
        s.makenote(pitch=20, velocity=vel, duration=dur, channel=1)
        s.makenote(pitch=21, velocity=vel, duration=dur, channel=1)
        s.makenote(pitch=22, velocity=vel, duration=dur, channel=1)
        s.makenote(pitch=23, velocity=vel, duration=dur, channel=1)
        s.makenote(pitch=24, velocity=vel, duration=dur, channel=1)
        s.makenote(pitch=25, velocity=vel, duration=dur, channel=1)
        s.makenote(pitch=26, velocity=vel, duration=dur, channel=1)
        s.makenote(pitch=27, velocity=vel, duration=dur, channel=1)
        s.makenote(pitch=28, velocity=vel, duration=dur, channel=1)
        s.makenote(pitch=29, velocity=vel, duration=dur, channel=1)
        s.makenote(pitch=40, velocity=vel, duration=dur, channel=1)
        s.makenote(pitch=81, velocity=vel, duration=dur, channel=1)
        s.makenote(pitch=82, velocity=vel, duration=dur, channel=1)
        s.makenote(pitch=83, velocity=vel, duration=dur, channel=1)
        s.makenote(pitch=84, velocity=vel, duration=dur, channel=1)
        s.makenote(pitch=85, velocity=vel, duration=dur, channel=1)
        s.makenote(pitch=86, velocity=vel, duration=dur, channel=1)
        s.makenote(pitch=87, velocity=vel, duration=dur, channel=1)
        s.makenote(pitch=88, velocity=vel, duration=dur, channel=1)
        s.makenote(pitch=89, velocity=vel, duration=dur, channel=1)

    if time_counter == 85:
        speech.play()

    if time_counter == 90:
        s.ctlout(64, 0)
        pat.stop()

    # desacelerando vers très aigu pedal on
    #s.ctlout(64, 127)

# Actual time counter
global_time = Pattern(time_events, 1).play()

# Patterns:
pitch = Phasor(freq=1, mul=48, add=40)

# Global variable to count the down and up beats.
count = 0
mul_count = 0
freq_count = 0

def midi_event():
    global count, mul_count, pitch, freq_count, s
    
    # Retrieve the value of the pitch audio stream and convert it to an int.
    pit = int(pitch.get())
   
    # each 23 seconds
    mul_count = mul_count + 1
    if mul_count == 23:
        #pitch.mul = pitch.mul + 1
        pitch.add = pitch.add + 1
        print("MUL: ", pitch.mul)
        mul_count = 0;

    # each 35 seconds
    freq_count = freq_count + 1
    if freq_count == 35:
        pitch.freq = random.randint(1,30)
        print("FREQ: ", pitch.freq)
        freq_count = 0;

    # If the count is 0 (down beat), play a louder and longer event, otherwise
    # play a softer and shorter one.
    if count == 0 and random.randint(0,1) < .5: # half chance
        vel = random.randint(40, 70)
        dur = random.randint(9,2000)
        #chord
        s.makenote(pitch=pit+12, velocity=vel, duration=dur, channel=1)
        s.makenote(pitch=pit+14, velocity=vel, duration=dur, channel=1)
        s.makenote(pitch=pit+16, velocity=vel, duration=dur, channel=1)
    else:
        vel = random.randint(50, 80)
        dur = random.randint(50, 80)
        s.makenote(pitch=pit, velocity=vel, duration=dur, channel=1)

    # Increase and wrap the count to generate a 4 beats sequence.
    count = (count + 1) % random.randint(12,13)

    print("pitch: %d, velocity: %d, duration: %d" % (pit, vel, dur))

def start_pat():
    global pat
    pat.play()

# Generates a MIDI event every 125 milliseconds.
pat = Pattern(midi_event, pat_time)
a = CallAfter(start_pat, 30)

s.gui(locals())
