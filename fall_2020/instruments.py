import random
from pyo import *

########### INSTRUMENTS ##########################

class Speech():
    def __init__(self, soundfile=[], loop=False, mul=.5, fadein=.01, fadeout=.01, duration=0, chnl=0, inc=1):
        self.amp = Fader(fadein=fadein, fadeout=fadeout, dur=duration, mul=mul)
        self.chnl = chnl
        self.inc = inc
        self.soundfile = soundfile
        self.soundfile_to_play = random.choice(soundfile)
        self.player = SfPlayer(self.soundfile_to_play, mul=[self.amp/2., self.amp/1.95], loop=loop).stop()
        self.player_rev = Freeverb(self.player, size=[.3,.25], damp=.6, bal=.4, mul=.8).out(chnl=self.chnl, inc=self.inc)

    def setDur(self, dur):
        self.amp.dur = dur
        return self

    def play(self):
        self.player.setSound(random.choice(self.soundfile))
        self.player.play()
        self.amp.play()
        return self

    def stop(self):
        self.amp.stop()
        return self

    def getOut(self):
        return self.amp

class IntroSines():
    def __init__(self, freq=[3000, 3000.01, 3000.03], harms=400, mul=.8):
        self.amp = Fader(fadein=10, fadeout=10, dur=0, mul=mul)
        self.sines = Blit(freq=freq, harms=harms, mul=self.amp * .01).out()
        self.rev = Freeverb(self.sines, size=.84, damp=.87, bal=.9, mul=self.amp * .2).out()

    def setDur(self, dur):
        self.amp.dur = dur
        return self

    def play(self):
        self.amp.play()
        return self

    def stop(self):
        self.amp.stop()
        return self

    def getOut(self):
        return self.amp


class HighFreq():
    def __init__(self, freq=[11200, 11202], dur=.4, mul=.4):
        self.amp = Fader(fadein=.01, fadeout=.01, dur=dur, mul=mul)
        self.sine = SineLoop(freq=freq, mul=self.amp * .05).out()
        self.rev = Freeverb(self.sine, size=.84, damp=.87, bal=.9, mul=self.amp * .2).out()

    def setDur(self, dur):
        self.amp.dur = dur
        return self

    def play(self):
        self.amp.play()
        return self

    def stop(self):
        self.amp.stop()
        return self

    def getOut(self):
        return self.amp

class SmoothNoise():
    def __init__(self, dur=1.3, mul=.4):
        self.amp = Fader(fadein=.1, fadeout=.01, dur=dur, mul=mul)
        self.noise = PinkNoise(self.amp * .01).mix(2).out()

    def setDur(self, dur):
        self.amp.dur = dur
        return self

    def play(self):
        self.amp.play()
        return self

    def stop(self):
        self.amp.stop()
        return self

    def getOut(self):
        return self.amp

    def setInput(self, x, fadetime=.001):
        self.input.setInput(x, fadetime)

class MyFreezing():
    def __init__(self, mul=1):
        global s
        f = 'sound_bank/444166__cloe-king__wine-glass-ring.wav'
        f_len = sndinfo(f)[1]
        #s.startoffset = f_len

        self.globalamp = Delay(Fader(fadein=100, dur=0).play(), delay=f_len, maxdelay=f_len)

        src = SfPlayer(f, loop=True, mul=0.8)

        # When this number increases, more analysis windows are randomly used.
        spread = Sig(0.1, mul=0.1)

        # The normalized position where to freeze in the sound.
        index = Sig(0.25, add=Noise(spread))

        self.pva = PVAnal(src, size=4096, overlaps=8)
        self.pvb = PVBuffer(self.pva, index, pitch=1.02)
        self.pvv = PVVerb(self.pvb, revtime=0.999, damp=0.995)
        self.pvs = PVSynth(self.pvv, mul=0.3)
        self.rev = STRev(self.pvs, roomSize=1, revtime=1)
        self.outsig= Delay(self.rev, delay=.1, feedback=0.2, mul=self.globalamp * mul).stop()

    def play(self):
        self.pvb.play()
        self.outsig.out()

    def stop(self):
        self.outsig.stop()

    def refresh(self):
        self.play()
