from pyo import *
import random
import os
from instruments import *

s = Server(audio='jack', duplex=0, nchnls=2)
s.setMidiOutputDevice(99)
s.boot()

m = MyFreezing()
m2 = MyFreezing()
m3 = MyFreezing()
m.stop()
m2.stop()
m3.stop()

# Open pedal
s.ctlout(64, 127)

################# BEGIN  GESTURE 00 ##################

intro_speech = Speech(['intro.wav'])
sines = IntroSines()

def g00():
    global intro_speech
    intro_speech.play()

g00Time = Metro(time=Randi(31, 39)).stop()
g00Func = TrigFunc(g00Time, g00)

################# BEGIN  GESTURE 01 ##################

piano_flag = True

def g01():
    global piano_flag
    sines.play()
    intro_speech.stop()
    if piano_flag == True:
       s.makenote(pitch=22, velocity=random.randint(30, 45), duration=20000)
       s.makenote(pitch=79, velocity=random.randint(60, 70), duration=20000)
       s.makenote(pitch=91, velocity=random.randint(70, 90), duration=20000)
    m.pvb.setPitch(random.uniform(0.90, 1.1))
    m.refresh()

g01Time = Metro(time=Randi(20, 35)).stop()
g01Func = TrigFunc(g01Time, g01)

################# BEGIN  GESTURE 02 ##################

high = HighFreq(mul=.05)

def g02():
    global high
    high.play()

g02Time = Metro(time=Randi(10, 30)).stop()
g02Func = TrigFunc(g02Time, g02)

################# BEGIN  GESTURE 03 ##################

snoise = SmoothNoise(mul=.25, dur=0.8)

def g03():
    global snoise
    snoise.play()

g03Time = Metro(time=Randi(10, 30)).stop()
g03Func = TrigFunc(g03Time, g03)

################# BEGIN  GESTURE 04 ##################

def g04():
    s.makenote(pitch=22, velocity=random.randint(60, 70), duration=20000)
    s.makenote(pitch=79, velocity=random.randint(70, 90), duration=20000)
    s.makenote(pitch=91, velocity=random.randint(90, 100), duration=20000)
    m.pvb.setPitch(random.uniform(0.9, 1.1))
    m2.pvb.setPitch(random.uniform(0.9, 1.1))
    m3.pvb.setPitch(random.uniform(0.9, 1.1))
    m.refresh()
    m2.refresh()
    m3.refresh()
    # send midi note

g04Time = Metro(time=Randi(20, 25)).stop()
g04Func = TrigFunc(g04Time, g04)

###################### SCORE ########################

time = -1

# Random speech to be called
speech_random = Speech(soundfile=os.listdir("44_statements"))
speech_random_time = Metro(time=Randi(25, 40)).stop()
speech_random_func = TrigFunc(speech_random_time, speech_random.play)

# Random speech to be called 2
speech_random2 = Speech(soundfile=os.listdir("44_statements"))
speech_random_time2 = Metro(time=Randi(10, 23)).stop()
speech_random_func2 = TrigFunc(speech_random_time2, speech_random2.play)

# GPT2 texts right before interlude
interlude_text = Speech(os.listdir('texts_speech'))
interlude_text_time = Metro(time=Randi(25, 40)).stop()
interlude_text_func = TrigFunc(interlude_text_time, interlude_text.play)

def score():
    global time, m, m2, m3, interlude_text, piano_flag, g01Time
    time += 1
    high.setDur(random.uniform(0.5, 1.5))
    snoise.setDur(random.uniform(0.5, 1.5))

    if time == 1:
        print(time)
        m.play() 
        g00Time.play()
    
    if time == 50:
        print(time)
        m2.play() 
        g01Time.play()

    if time == 80:
        print(time)
        g00Time.stop()

    if time == 120:
        print(time)
        piano_flag = False

    if time == 140:
        print(time)
        speech_random_time.play()

    ## Two minutes no piano only flute and voice
    if time == 200:
        print(time)
        g04Time.play() # starts new piano with low notes
        g01Time.stop() # stops initial piano
        speech_random_time.stop()
        m3.play() 
    
    if time == 260:
        print(time)
        g02Time.play() # high pitch

    if time == 270:
        print(time)
        interlude_text_time.play()

    if time == 280:
        print(time)
        g04Time.stop() # stops all piano
        g03Time.play()  # snoise
        speech_random_time.setTime(Randi(5, 10)) # overlapping voices
        speech_random_time.play() # overlapping voices

    # stop everything but high/snoise and call the serial (part3) script
    if time == 300:
        speech_random_time2.play()
        
    if time == 315:
        vel = 50
        dur = 2000

        s.ctlout(64, 127)

        s.makenote(pitch=20, velocity=vel, duration=dur, channel=1)
        s.makenote(pitch=22, velocity=vel, duration=dur, channel=1)
        s.makenote(pitch=24, velocity=vel, duration=dur, channel=1)
        s.makenote(pitch=26, velocity=vel, duration=dur, channel=1)
        s.makenote(pitch=28, velocity=vel, duration=dur, channel=1)
        s.makenote(pitch=80, velocity=vel, duration=dur, channel=1)
        s.makenote(pitch=82, velocity=vel, duration=dur, channel=1)
        s.makenote(pitch=84, velocity=vel, duration=dur, channel=1)
        s.makenote(pitch=86, velocity=vel, duration=dur, channel=1)
        s.makenote(pitch=88, velocity=vel, duration=dur, channel=1)

        print(time)
        m2.stop()
        m3.stop()
        m.stop()
        sines.stop()
        g01Time.stop()
        g02Time.stop()
        g03Time.stop()
        speech_random_time.stop()
        speech_random_time2.stop()
        interlude_text_time.stop()

mainTime = Metro(time=1).play()
mainFunc = TrigFunc(mainTime, score)

s.gui(locals())

