import random
from pyo import *
from instruments import Speech

pm_list_devices()

s = Server(audio='jack')

# Open all MIDI output devices.
s.setMidiOutputDevice(99)

# Then boot the Server.
s.boot()

speech_intro = Speech(['intro.wav'])
speech_intro.play()

# close pedal
s.ctlout(64, 0)

note_vel_min = 20
note_vel_max = 40

# set random-ish pattern time
pat_time = XnoiseDur(dist=11, min=5, max=10)
speech = Speech(['restart.wav'])

time_counter = 0
def time_events():
    global s, time_counter, pat_time, pat
    time_counter = time_counter + 1
    print(time_counter)
    print((pat_time.min, pat_time.max))
    
    # bass resonant slow
    # TODO: midi vel down
#    if time_counter == 45:
#        # sustain pedal on
#        s.ctlout(64, 127)
#        pat_time.dist = 0
#        pat_time.max = 15
#        pat_time.min = 8

    # 1:30
    if time_counter == 10:
       pat_time.max = 10 
       pat_time.min = .5 
  
    if time_counter == 20:
        pat_time.max = 5 
        pat_time.min = .5 

    if time_counter > 30 and pat_time.min > .1:
        pat_time.max = pat_time.min
        pat_time.min = pat_time.min - .025

    if time_counter == 40:
        s.ctlout(64, 127)

    if time_counter == 50:
        pat_time.max = 3
        pat_time.min = 3
        vel = 10
        dur = 2000
        s.makenote(pitch=30, velocity=vel, duration=dur, channel=1)
        s.makenote(pitch=31, velocity=vel, duration=dur, channel=1)
        s.makenote(pitch=32, velocity=vel, duration=dur, channel=1)
        s.makenote(pitch=33, velocity=vel, duration=dur, channel=1)
        s.makenote(pitch=34, velocity=vel, duration=dur, channel=1)
        s.makenote(pitch=35, velocity=vel, duration=dur, channel=1)
        s.makenote(pitch=36, velocity=vel, duration=dur, channel=1)
        s.makenote(pitch=37, velocity=vel, duration=dur, channel=1)
        s.makenote(pitch=38, velocity=vel, duration=dur, channel=1)
        s.makenote(pitch=39, velocity=vel, duration=dur, channel=1)
        s.makenote(pitch=40, velocity=vel, duration=dur, channel=1)
        s.makenote(pitch=41, velocity=vel, duration=dur, channel=1)
        s.makenote(pitch=42, velocity=vel, duration=dur, channel=1)
        s.makenote(pitch=43, velocity=vel, duration=dur, channel=1)
        s.makenote(pitch=44, velocity=vel, duration=dur, channel=1)
        s.makenote(pitch=45, velocity=vel, duration=dur, channel=1)
        s.makenote(pitch=46, velocity=vel, duration=dur, channel=1)
        s.makenote(pitch=47, velocity=vel, duration=dur, channel=1)
        s.makenote(pitch=48, velocity=vel, duration=dur, channel=1)
        s.makenote(pitch=49, velocity=vel, duration=dur, channel=1)

    if time_counter == 63:
        speech.play()


    if time_counter == 70
        pat.stop()
        subprocess.call("python3 part3.py", shell=True)

    # desacelerando vers très aigu pedal on
    #s.ctlout(64, 127)

# Actual time counter
global_time = Pattern(time_events, 1).play()

# patterns:
# freq = 9, mul = 48, add = 36
pitch = Phasor(freq=1, mul=48, add=40)

# Global variable to count the down and up beats.
count = 0
mul_count = 0
freq_count = 0

# make a class
def midi_event():
    global count, mul_count, pitch, freq_count, s
    
    # Retrieve the value of the pitch audio stream and convert it to an int.
    pit = int(pitch.get())
   
    # each 23 seconds
    mul_count = mul_count + 1
    if mul_count == 23:
        #pitch.mul = pitch.mul + 1
        pitch.add = pitch.add + 1
        print("MUL: ", pitch.mul)
        mul_count = 0;

    # each 35 seconds
    freq_count = freq_count + 1
    if freq_count == 35:
        pitch.freq = random.randint(1,30)
        print("FREQ: ", pitch.freq)
        freq_count = 0;

    # If the count is 0 (down beat), play a louder and longer event, otherwise
    # play a softer and shorter one.
    if count == 0 and random.randint(0,1) < .5: # half chance
        vel = random.randint(20, 30)
        dur = random.randint(9,2000)
        #chord
        s.makenote(pitch=pit+12, velocity=vel, duration=dur, channel=1)
        s.makenote(pitch=pit+14, velocity=vel, duration=dur, channel=1)
        s.makenote(pitch=pit+16, velocity=vel, duration=dur, channel=1)
    else:
        vel = random.randint(40, 50)
        dur = random.randint(40, 50)
        s.makenote(pitch=pit, velocity=vel, duration=dur, channel=1)

    # Increase and wrap the count to generate a 4 beats sequence.
    count = (count + 1) % random.randint(12,13)


    print("pitch: %d, velocity: %d, duration: %d" % (pit, vel, dur))


def start_pat():
    global pat
    pat.play()

# Generates a MIDI event every 125 milliseconds.
pat = Pattern(midi_event, pat_time)
a = CallAfter(start_pat, 30)


s.gui(locals())
