import random
from pyo import *
from instruments import *
import time

pm_list_devices()

s = Server(audio='jack', duplex=0, nchnls=2)

# Open all MIDI output devices.
s.setMidiOutputDevice(99)

# Then boot the Server.
s.boot()

s.ctlout(64, 127)
speech_start = Speech(['mary.wav'], loop=True)
speech_start.play()

# Kinderstück serial sequence
notes_seq = [3, 4, 0, 11, 10, 1, 2, 9, 8, 7, 6, 5]

index = 0
index2 = 0
index3 = 0
pedal_flag = True

def intro_event():
    global s, pat, speech_start
    # close pedal
    s.ctlout(64, 0)
    pat.play()
    speech_start.stop()

def midi_event():
    global notes_seq, index, pat2, pat, s

    index = index + 1
    n, d = divmod(index, 12)
    print(index, n, d)
   
    vel = random.randint(25, 35)
    dur = random.randint(20, 1000)

    octave = random.choice([48, 60])
    s.makenote(pitch=notes_seq[d]+octave, velocity=vel, duration=dur, channel=1)
    if n == 1:
        s.makenote(pitch=notes_seq[d]+octave+12, velocity=vel, duration=dur, channel=1)

    print("pitch: %d, velocity: %d, duration: %d" % (notes_seq[d], vel, dur))

    if n == 2:
        pat2.play()
        final_event2Time.play()

event2_part2 = 0
event2_flag = False

def midi_event2():
    global notes_seq, index2, pat2, pat, event2_part2, event2_flag, pedal_flag
    if pedal_flag == True:
        s.ctlout(64, 127)
        pedal_flag = False
   
    vel = random.randint(20, 30)
    dur = 100

    octave = random.choice([60, 72])
    s.makenote(pitch=notes_seq[index2]+octave, velocity=vel, duration=dur, channel=1)
    if event2_flag == True:
        s.makenote(pitch=notes_seq[index2]+octave-14, velocity=vel, duration=dur, channel=1)

    print("pitch: %d, velocity: %d, duration: %d" % (notes_seq[index2], vel, dur))

    index2 = index2 + 1
    if index2 == 12:
        final_eventTime.play()
        index2 = 0
        event2_flag = True

    event2_part2 = event2_part2 + 1
    if event2_part2 == 48:
        pat3.play()

speech_final = Speech(['insult.wav'])
def midi_event3():
    global notes_seq, index3, pat3, speech_final
   
    vel = random.randint(20, 30)
    dur = 100

    octave = random.choice([24, 84, 96])
    s.makenote(pitch=notes_seq[index3]+octave, velocity=vel, duration=dur, channel=1)
    s.makenote(pitch=notes_seq[index3]+octave-14, velocity=vel, duration=dur, channel=1)
    s.makenote(pitch=notes_seq[index3]+octave-16, velocity=vel, duration=dur, channel=1)
    print("pitch: %d, velocity: %d, duration: %d" % (notes_seq[index3], vel, dur))

    index3 = index3 + 1
    if index3 == 12:
        index3 = 0
        pat.stop()
        pat2.stop()
        pat3.stop()
        speech_final.play()

snoise = SmoothNoise(mul=.25, dur=0.3)
high = HighFreq(mul=0.5)

def final_event():
    global high
    high.setDur(random.uniform(0.3, 0.6))
    high.play()

final_eventTime = Metro(time=Randi(10, 20)).stop()
final_eventFunc = TrigFunc(final_eventTime, final_event)

def final_event2():
    global snoise
    snoise.setDur(random.uniform(0.3, 0.6))
    snoise.play()

final_event2Time = Metro(time=Randi(10, 20)).stop()
final_event2Func = TrigFunc(final_event2Time, final_event2)

# set random-ish pattern time
pat_time = XnoiseDur(dist=11, min=.1, max=8)

# Generates a MIDI event every 125 milliseconds.
pat = Pattern(midi_event, pat_time)

# set random-ish pattern time
pat_time2 = XnoiseDur(dist=11, min=0.5, max=10)

# Generates a MIDI event every 125 milliseconds.
pat2 = Pattern(midi_event2, pat_time2)

# set random-ish pattern time
pat_time3 = XnoiseDur(dist=11, min=.1, max=4)

# Generates a MIDI event every 125 milliseconds.
pat3 = Pattern(midi_event3, pat_time3)

a = CallAfter(intro_event, random.randint(30,40))

s.gui(locals())
