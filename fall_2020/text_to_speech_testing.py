import os
import random

os.environ["GOOGLE_APPLICATION_CREDENTIALS"]="credentials.json"

from google.cloud import texttospeech

# Instantiates a client
client = texttospeech.TextToSpeechClient()

statements = ['Another way that emotions can happen in a crowd is from previously unspecific \
empathic reactions to stimuli. Annie, an 18-year-old classically trained \
musician, got her first emergency beat early, spooked by the sounds.']

#statements = ['You’re making total sense.', 'I understand how you feel.', 'You must feel so hopeless.', 'I just feel such despair in you when you talk about this.', 'You’re in a tough spot here.', 'I can feel the pain you feel.', 'The world needs to stop when you’re in this much pain.', 'I wish you didn’t have to go through that.', 'I’m on your side here.', 'I wish I could have been with you in that moment.', 'Oh, wow, that sounds terrible.', 'You must feel so helpless.', 'That hurts me to hear that.', 'I support your position here.', 'I totally agree with you.', 'You are feeling so trapped!', 'You are making total sense.', 'That sounds like you felt really disgusted!', 'No wonder you’re upset.', 'I’d feel the same way you do in your situation.', 'I think you’re right.', 'I see. Let me summarize: What you’re thinking here is…', 'You are in a lot of pain here. I can feel it.', 'It would be great to be free of this.', 'That must have annoyed you.', 'That would make me mad too.', 'That sounds infuriating.', 'That sounds frustrating.', 'That is very scary.', 'Well I agree with most of what you’re saying.', 'I would have also been disappointed by that.', 'That would have hurt my feelings also.', 'That would make me sad too.', 'POOR BABY!', 'Wow, that must have hurt.', 'I understand what you’re feeling.', 'You are making a lot of sense to me.', 'Okay, I think I get it. So what you’re feeling is…', 'Let me try to paraphrase and summarize what you’re saying. You’re saying…', 'I would have trouble coping with that.', 'What I admire most about what you’re doing is…', 'That would make me feel insecure.', 'That sounds a little frightening.', 'Tell me what you see as your choices here.']

#statements = ['You must feel so helpless. Think of Mary, whom she will hold most dear: Action, \
#noise, retreat, purpose, right and wrong, fear, wonder, grief, gratitude, \
#devotion, status, perverted idealism, boundary usage, guilt, wrongdoing, \
#beautiful imaginations, planned states, imaginal behavior, finds ahead, offered \
#knowledge, dietary patterns with dear remembering, ancestors, grandchildren, \
#singing, drama, fencing (fat cats killed babies’s babies completely \
#uncountably), attacks. Victimization. Victimization. Victimization.']

#statements = ['You must feel so helpless. Think of Mary, whom she will hold most dear: Action, \
#noise, retreat, purpose, right and wrong, fear, wonder, grief, gratitude, \
#devotion, status, perverted idealism, boundary usage, guilt, wrongdoing, \
#beautiful imaginations, planned states, imaginal behavior, finds ahead, offered \
#knowledge, dietary patterns with dear remembering, ancestors, grandchildren, \
#singing, drama, fencing (fat cats killed babies’s babies completely \
#uncountably), attacks. Victimization. Victimization. Victimization.']

#statements = ['Empathy works by increasing the activation of your empathic awareness in \
#others, which literally increases the effort of positive empathy work on their \
#part. Thus, if you activate empathy near others, empathically speaking, this \
#will help them feel more closely and empathically attached to you. It shouldn’t \
#be surprising then, that empathy interactions also have a direct relationship \
#to empathy intensity.']

#statements = ['The first nearly reliable way that emotions can arise is from direct experience \
#with others. You’ll remember the emotion you’re feeling in this session if you \
#experienced the same emotion in a previous session. Activation of your empathic \
#awareness—and possible neurologic and neurological effects—in others can affect \
#your ability to quality-quantify comebacks for others in your life.']

#statements = ['Oh, wow, that sounds terrible. Don’t waste your time learning style by \
#imitating Beyoncé or learn to enliven yourself with sensory terms like \
#great’Ease, Adorable, Mellonicious! — because these styles don’t exist yet. There \
#will always be trouble on the underground level — even within the own \
#organization, and nobody with CREEPY IS BOYS (okay, so maybe Beyoncé isn’t some \
#Do-it-right revolutionary self).']

#statements = ["""I see. Let me summarize: What you’re thinking here is that you can make money \
#from doing nothing, and the only way to make money is to sell your services to \
#companies. You have a very good point. The problem is that your idea is an idea \
#that is not particularly interesting. It's an idea that does not make any \
#money. It's a bad idea. That's why you're not getting rich, and that's why \
#you're not doing anything interesting. You're just sitting around."""]

#statements = ["""I see. Let me summarize: What you’re thinking here is that I'm not so bad. No. \
#You're thinking I'm a shit-stain. You're thinking I'm a terrible person. \
#You're thinking I'm a creep. You're thinking I'm a monster. You're thinking I'm \
#a fucking lunatic. You're thinking I'm a dumbass. No. You're thinking I'm a \
#good person. You're thinking I'm a good person.  You're thinking I'm a good \
#person."""]

#pick = str(random.choice(statements))
index = 0
ssml_pre = '<speak><prosody rate="90%" pitch="-5%"><emphasis level="strong">'
ssml_post = '</emphasis></prosody></speak>'

for pick in statements:
    print(pick)
    ssml = ssml_pre + pick + ssml_post
    
    # Set the text input to be synthesized
    synthesis_input = texttospeech.SynthesisInput(ssml=ssml)
    
    # Build the voice request, select the language code ("en-US") 
    # ****** the NAME
    # and the ssml voice gender ("neutral")

    voices_list = ['en-AU-Wavenet-B', 'en-AU-Wavenet-A', 'en-AU-Wavenet-C',
                   'en-AU-Wavenet-D', 'en-IN-Wavenet-A', 'en-IN-Wavenet-B', 'en-IN-Wavenet-C',
                   'en-IN-Wavenet-D', 'en-GB-Wavenet-A', 'en-GB-Wavenet-B', 'en-GB-Wavenet-C',
                   'en-GB-Wavenet-D', 'en-GB-Wavenet-F', 'en-US-Wavenet-A', 'en-US-Wavenet-B',
                   'en-US-Wavenet-C', 'en-US-Wavenet-D', 'en-US-Wavenet-E', 'en-US-Wavenet-F',
                   'en-US-Wavenet-G', 'en-US-Wavenet-H', 'en-US-Wavenet-I', 'en-US-Wavenet-J' ]
    #pick_voice = str(random.choice(voices_list))
    pick_voice = 'en-US-Wavenet-F'

    voice = texttospeech.VoiceSelectionParams(
        language_code='en-US',
        name=pick_voice)
        #ssml_gender=texttospeech.SsmlVoiceGender.FEMALE)
    
    # Select the type of audio file you want returned
    audio_config = texttospeech.AudioConfig(
        audio_encoding=texttospeech.AudioEncoding.LINEAR16)
    
    # Perform the text-to-speech request on the text input with the selected
    # voice parameters and audio file type
    response = client.synthesize_speech(input=synthesis_input, voice=voice, audio_config=audio_config)
    
    # The response's audio_content is binary.
    #audio_file = str(index+1) + '_' + pick_voice + '.wav'
    audio_file = '18.wav'

    with open(audio_file, 'wb') as out:
        # Write the response to the output file.
        out.write(response.audio_content)
        print('Audio content written to file %s' % audio_file)
    index = index + 1
