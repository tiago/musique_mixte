import random
import mido

port = mido.open_output('CH345:CH345 MIDI 1 20:0')

PLAY_MIDI = True
PLAY_SELECTOR = False
DEBUSSY = [1, 3, 5, 7, 9, 11]
SCHOENBERG = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]

# mappings
# - midi dur, velocity, octave, pattern frequency, mode
# - synth selector: lorenz x ring

#################### OSC SETUP ##########################

osc = OscReceive(port=10001, address=['/pitch'])

#################### END OSC SETUP ######################

########## MIDI ######################################

def sendoff(args):
    note = args[0]
    velocity = args[1]
    msgoff = mido.Message('note_off',channel=1, note=note, velocity=velocity)
    port.send(msgoff)

note = random.randint(10,90)
velocity = random.randint(30,94)
dur = random.uniform(0.25, 1)
ca = CallAfter(sendoff, dur, (note, velocity)).stop()
counter = 0
mu = osc.get(identifier='/pitch', all=False)

def midi_event():
    global mu, counter, ca, dur, note, velocity, osc
    counter = counter + 1
    # change register and velocity each x_interc interactions
    if counter == 1:
        meditation = int(osc.get(identifier='/pitch', all=False))
        octave_factor = int(rescale(osc.get(identifier='/pitch', all=False), 0, 100, 1, 7))
        note_factor = int(rescale(osc.get(identifier='/pitch', all=False), 0, 100, 21, 108))
        velocity_factor = int(rescale(osc.get(identifier='/pitch', all=False), 0, 100, 99, 10))
        velocity = velocity_factor
        counter = 0
    if  meditation > 50:
        scale = DEBUSSY 
    else:
        scale = SCHOENBERG
        
    note = random.choice(scale) + note_factor #FIXME: of course doesnt work
    print note
    dur_factor = rescale(osc.get(identifier='/pitch', all=False), 0, 100, 0.001, 0.005)
    dur = dur_factor
    
    msg = mido.Message('note_on', channel=1, velocity=velocity, note=note)
    port.send(msg)
    ca.setArg((note, velocity))
    ca.setTime(dur)
    ca.play()

r = Scale(osc['/pitch'], 0, 100, 0.01, 1, exp=2)

#################### END MIDI ###########################

lor_mul_out = Sig(Scale(osc['/pitch'], 0, 100, 0, 0.2, 0.01))
lor_mul = Lorenz(pitch=.003, stereo=True, mul=.2, add=.2)
lor = Lorenz(pitch=[.4,.38], mul=lor_mul_out)

tab_m = HarmTable([1,0,0,0,0,.3,0,0,0,0,0,.2,0,0,0,0,0,.1,0,0,0,0,.05]).normalize()
tab_p = HarmTable([1,0,.33,0,.2,0,.143,0,.111])

class Ring:
    def __init__(self, fport=250, fmod=100, amp=.3):
        self.mod = Osc(tab_m, freq=fmod, mul=amp)
        self.port = Osc(tab_p, freq=fport, mul=self.mod)

    def out(self):
        self.port.out()
        return self

    def sig(self):
        return self.port

lf = Sine(.03, mul=.5, add=1)
rg = Ring(fport = [random.choice([62.5,125,187.5,250]) * random.uniform(.98,1.02) for i in range(8)],
          fmod = lf * [random.choice([25,50,75,100]) * random.uniform(.98,1.02) for i in range(8)],
          amp = 0.1)
res = Waveguide(rg.sig(), freq=[30.1,60.05,119.7,181,242.5,303.33, 599.8], dur=30, mul=.1)

lf_sel = Sig(Scale(osc['/pitch'], 0, 100, 0, 1, exp=0.25))

#################### END SYNTH ##########################

################ MAIN #################

if PLAY_MIDI:
    pat = Pattern(midi_event, r).play()

if PLAY_SELECTOR:
    sel = Selector(inputs=[lor, res], voice=lf_sel).out()

