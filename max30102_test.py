import max30102
import hrcalc

while True:
    m = max30102.MAX30102()
    red, ir = m.read_sequential()
    o = hrcalc.calc_hr_and_spo2(ir[:100], red[:100])
    print(o)
