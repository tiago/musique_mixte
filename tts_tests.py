from gtts import gTTS
import os
import markovify

################ Markov #####################

# Get raw text as string.
with open("oracles.txt") as f:
    text = f.read()

# Build the model.
text_model = markovify.Text(text, state_size=2)

# Print three randomly-generated sentences of no more than 280 characters
for i in range(10):
    text_out = text_model.make_short_sentence(100)
    print(text_out)
    tts = gTTS(text=text_out, slow=False, lang='en-uk')
    tts.save("out.mp3")
    os.system("mplayer -ao jack out.mp3")
