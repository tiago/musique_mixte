from pyo import *
import random
s = Server(audio='jack')
s.setMidiOutputDevice(99)
s.boot()


high_pitch_sines = Blit(freq=[3000, 3000.01, 3000.03], harms=400, mul=.005).out()
#high_pitch_sines2 = Blit(freq=[4.3768, 4.3768], harms=12.5538, mul=0.8846).out()
high_pitch_sines.ctrl()
#high_pitch_sines2.ctrl()

s.ctlout(64, 127)

########### INSTRUMENTS ##########################

class HighFreq():
    def __init__(self, freq=[11200, 11202], dur=.4, mul=.5):
        self.amp = Fader(fadein=.01, fadeout=.01, dur=dur, mul=mul)
        self.sine = SineLoop(freq=freq, mul=self.amp * .05).out()
        self.rev = Freeverb(self.sine, size=.84, damp=.87, bal=.9, mul=self.amp * .2).out()

    def setDur(self, dur):
        self.amp.dur = dur
        return self

    def play(self):
        self.amp.play()
        return self

    def stop(self):
        self.amp.stop()
        return self

    def getOut(self):
        return self.amp

class SmoothNoise():
    def __init__(self, dur=1.3, mul=.5):
        self.amp = Fader(fadein=.1, fadeout=.01, dur=dur, mul=mul)
        self.noise = PinkNoise(self.amp * .01).mix(2).out()

    def setDur(self, dur):
        self.amp.dur = dur
        return self

    def play(self):
        self.amp.play()
        return self

    def stop(self):
        self.amp.stop()
        return self

    def getOut(self):
        return self.amp

    def setInput(self, x, fadetime=.001):
        self.input.setInput(x, fadetime)

class MyFreezing():
    def __init__(self, mul=1):
        global s
        f = 'sound_bank/444166__cloe-king__wine-glass-ring.wav'
        f_len = sndinfo(f)[1]
        #s.startoffset = f_len

        self.globalamp = Delay(Fader(fadein=1, dur=0).play(), delay=f_len, maxdelay=f_len)

        src = SfPlayer(f, loop=True, mul=0.5)

        # When this number increases, more analysis windows are randomly used.
        spread = Sig(0.1, mul=0.1)

        # The normalized position where to freeze in the sound.
        index = Sig(0.25, add=Noise(spread))

        self.pva = PVAnal(src, size=4096, overlaps=8)
        self.pvb = PVBuffer(self.pva, index, pitch=1.02)
        self.pvv = PVVerb(self.pvb, revtime=0.999, damp=0.995)
        self.pvs = PVSynth(self.pvv, mul=0.3)
        self.rev = STRev(self.pvs, roomSize=1, revtime=1)
        self.outsig= Delay(self.rev, delay=.1, feedback=0.2, mul=self.globalamp * mul).stop()

    def play(self):
        self.pvb.play()
        self.outsig.out()

    def stop(self):
        self.outsig.stop()

    def refresh(self):
        self.play()

m = MyFreezing()
m2 = MyFreezing()
m3 = MyFreezing()
m.stop()
m2.stop()
m3.stop()

################# BEGIN  GESTURE 01 ##################

def g01():
    s.makenote(pitch=22, velocity=random.randint(10, 25), duration=20000)
    s.makenote(pitch=79, velocity=random.randint(50, 70), duration=20000)
    s.makenote(pitch=91, velocity=random.randint(50, 70), duration=20000)
    m.pvb.setPitch(random.uniform(0.90, 1.1))
    m.refresh()
    # send midi note

g01Time = Metro(time=Randi(12, 25)).stop()
g01Func = TrigFunc(g01Time, g01)

################# BEGIN  GESTURE 02 ##################

high = HighFreq(mul=.05)

def g02():
    global high
    high.play()

g02Time = Metro(time=Randi(10, 30)).stop()
g02Func = TrigFunc(g02Time, g02)

################# BEGIN  GESTURE 03 ##################

snoise = SmoothNoise(mul=.25, dur=0.8)

def g03():
    global snoise
    snoise.play()

g03Time = Metro(time=Randi(10, 30)).stop()
g03Func = TrigFunc(g03Time, g03)

################# BEGIN  GESTURE 04 ##################

def g04():
    s.makenote(pitch=22, velocity=random.randint(40, 60), duration=20000)
    s.makenote(pitch=79, velocity=random.randint(40, 60), duration=20000)
    s.makenote(pitch=91, velocity=random.randint(20, 30), duration=20000)
    m.pvb.setPitch(random.uniform(0.9, 1.1))
    m2.pvb.setPitch(random.uniform(0.9, 1.1))
    m3.pvb.setPitch(random.uniform(0.9, 1.1))
    m.refresh()
    m2.refresh()
    m3.refresh()
    # send midi note

g04Time = Metro(time=Randi(5, 20)).stop()
g04Func = TrigFunc(g04Time, g04)

################# BEGIN  GESTURE 05 ##################

g05_player_rev = None

def g05():
    global high_pitch_sines, g05_player_rev
    high_pitch_sines.stop()
    g05_player = SfPlayer('9.wav', mul=0.5)
    g05_player_rev = Freeverb(g05_player, size=[.3,.2], damp=.5, bal=.3).out()

g05Time = Metro(time=Randi(1, 5)).stop()
g05Func = TrigFunc(g05Time, g05)

################ SCORE ########################

time = -1

def score():
    global time, m, m2, m3
    time += 1
    high.setDur(random.uniform(0.5, 1.5))
    snoise.setDur(random.uniform(0.5, 1.5))

    if time == 1:
        print(time)
        m.play() 
    
    if time == 50:
        print(time)
        m2.play() 
        g01Time.play()

    if time == 150:
        print(time)
        g01Time.stop() # stops initial piano
        g04Time.play() # starts new piano
        m3.play() 
    
    if time == 200:
        print(time)
        g02Time.play()
        m.stop()

    if time == 220:
        print(time)
        g03Time.play() 

    # no piano at all
    # 1 minute of instrumental duo + synth statements
    if time == 270:
        print(time)
        g04Time.stop() # stops all piano
        m2.stop()

    # stop everything, repeated synth statement, keep a single freezing loop
    if time == 330:
        print(time)
        g01Time.stop()
        g02Time.stop()
        g03Time.stop()
        g05Time.play()

mainTime = Metro(time=1).play()
mainFunc = TrigFunc(mainTime, score)
    

s.gui(locals())

