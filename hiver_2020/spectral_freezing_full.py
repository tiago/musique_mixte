from pyo import *
s = Server().boot()

class SmoothNoise():
    def __init__(self, dur=1.3, mul=.5):
        self.amp = Fader(fadein=.1, fadeout=.01, dur=dur, mul=mul)
        self.noise = PinkNoise(self.amp * .01).mix(2).out()

    def setDur(self, dur):
        self.amp.dur = dur
        return self

    def play(self):
        self.amp.play()
        return self

    def stop(self):
        self.amp.stop()
        return self

    def getOut(self):
        return self.amp

    def setInput(self, x, fadetime=.001):
        self.input.setInput(x, fadetime)

class HighFreq():
    def __init__(self, freq=[11200, 11202], dur=.4, mul=.5):
        self.amp = Fader(fadein=.01, fadeout=.01, dur=dur, mul=mul)
        self.sine = SineLoop(freq=freq, mul=self.amp * .05).out()
        self.rev = Freeverb(self.sine, size=.84, damp=.87, bal=.9, mul=self.amp * .2).out()

    def setDur(self, dur):
        self.amp.dur = dur
        return self

    def play(self):
        self.amp.play()
        return self

    def stop(self):
        self.amp.stop()
        return self

    def getOut(self):
        return self.amp

class MyFreezing():
    def __init__(self, mul=2):
        global s
        #f = SNDS_PATH+'/transparent.aif'
        #f = '341180__wdmatheson__wine-glass-high.wav'
        #f = '400909__inspectorj__voice-request-25-where-i-am.wav'
        #f = '263394__cellokratzer__orchestra-warm-up.wav'
        f = 'classicalrevrva2013-01-20t03.flac'
        f_len = sndinfo(f)[1]
        #s.startoffset = f_len
        
        src = SfPlayer(f, loop=True, mul=0.3)
        #src_performance = src.out()
        
        # When this number increases, more analysis windows are randomly used.
        spread = Sig(0.1, mul=0.1)
        
        # The normalized position where to freeze in the sound.
        index = Sig(0.25, add=Noise(spread))

        self.pva = PVAnal(src, size=1024, overlaps=8)
        self.pvb = PVBuffer(self.pva, index, pitch=1.0)
        self.pvb2 = PVBuffer(self.pva, index, pitch=1.01)
        self.pvb3 = PVBuffer(self.pva, index, pitch=0.97)
        self.amp = Fader(fadein=5, fadeout=5).stop()
        self.pvs = PVSynth([self.pvb, self.pvb2], mul=self.amp * mul)
        self.outsig = STRev(self.pvs, roomSize=4, revtime=100, mul=self.amp * mul)
        self.outsig.ctrl()
        #self.feedback = .2
        #self.outsig = Delay(self.rev, delay=.1, feedback=self.feedback, mul=self.amp).stop()

    def play(self):
        self.amp.play()
        self.pvb.play()
        self.pvb2.play()
        self.pvb3.play()
        self.outsig.out()

    def tail(self):
        self.amp.stop()

    def stop(self):
        self.outsig.stop()

    def refresh(self):
        self.tail()
        self.play()

m = MyFreezing()
m2 = MyFreezing()

flag = True

high = HighFreq(mul=.05)
noise = SmoothNoise(mul=.5)

def score():
    global m, m2, flag
    if flag:
        m.refresh()
        flag = False
    else:
        m2.refresh()
        flag = True

def randElements():
        noise.play()
        high.play()

mainTime = Metro(time=1).play()
mainFunc = TrigFunc(mainTime, score)

randTime = Metro(time=Randi(10, 20)).play()
randFunc = TrigFunc(randTime, randElements)

s.gui(locals())

