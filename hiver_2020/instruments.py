from pyo import *
s = Server(audio='jack').boot()

high_pitch_sines = Blit(freq=[3000, 3000.01], harms=400, mul=.1).out()

simple_drone = SineLoop([40, 40.1, 40.2], mul=.2).out()

brown_noise = BrownNoise(.05).mix(2).out()

# stones machine
a = ChenLee(pitch=.1, chaos=0.2, stereo=True, mul=.5, add=.5)
b = ChenLee(pitch=0.1, chaos=a, mul=0.5).out()



s.gui(locals())
