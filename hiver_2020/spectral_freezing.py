from pyo import *
#from NeuroPy import NeuroPy


s = Server(audio='jack').boot()

#f = './sound_bank/55955__ldk1609__violin-pizz-non-vib-a-2.aiff'
f = './sound_bank/159116__cms4f__cello-play-c-09.wav'
#f = './sound_bank/250954__thalamus-lab__scary-harmonic.wav'
f = './sound_bank/444166__cloe-king__wine-glass-ring.wav'
#f = './sound_bank/495376__camel7695__wine-glass-rough-tones.wav'

#bw=NeuroPy("/dev/rfcomm0")
#bw.start()

f_len = sndinfo(f)[1]

high_pitch_sines = Blit(freq=[3000, 3000.01], harms=100, mul=.1).out()
high_pitch_sines.ctrl()

heartbeat = Blit(freq=[2.09, 2.15], harms=100, mul=.1).out()
heartbeat.ctrl()


# Let the analysis buffer fill in.
#s.startoffset = f_len

# OSC setup
#osc = OscReceive(port=10001, address=['/attention', '/meditation', '/poorSignal'])
#meditation = int(osc.get(identifier='/meditation', all=False))

#sin = Sine(freq=SigTo(bw.meditation+200), mul=.5).out()

sin=Sine(freq=[3000,3003,3007,30014], mul=0.05).out()
sin.ctrl()

#f2_play = SfPlayer(f2, mul=0.7).out()
src = SfPlayer(f, mul=0.7)
o = src.mix(2).out()

# When this number increases, more analysis windows are randomly used.
spread = Sig(0.1, mul=0.1)
spread.ctrl(title="Spread")

# The normalized position where to freeze in the sound.
r = Xnoise(dist=9, x1=0.038, x2=0.01, freq=0.1)
index = Sig(0.25, add=Noise(spread))
index.ctrl(title="Position in Sound")

pva = PVAnal(src, size=1024, overlaps=8)
pvb = PVBuffer(pva, index, pitch=1.0, length=f_len)
pvs = PVSynth(pvb).out()

#r2 = Randi(0.98, 1.02, freq=5)
#pvt = PVTranspose(pvb, transpo=r2)
#pvs2 = PVSynth(pvt).out()
#pvv = PVVerb(pvt, revtime=0.65, damp=0.95)

## Global score
#def score():
#    global sin, index
#
#    print("MEDITATION:", bw.meditation)
#    print("ATTENTION:", bw.attention)
#    sin.setFreq(bw.meditation+200)
#    index.setValue(bw.meditation/1000.)
#
#mainTime = Metro(time=.01).play()
#mainFunc = TrigFunc(mainTime, score)
#
s.gui(locals())

