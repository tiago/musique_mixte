from pyo import *
from random import normalvariate, randint

with open('44.txt') as f:
    lines = [line.rstrip() for line in f]

print(lines)

def normal_choice(lst, mean=None, stddev=None):
    if mean is None:
        # if mean is not specified, use center of list
        mean = (len(lst) - 1) / 2

    if stddev is None:
        # if stddev is not specified, let list be -3 .. +3 standard deviations
        stddev = len(lst) / 6

    while True:
        index = int(normalvariate(mean, stddev) + 0.5)
        if 0 <= index < len(lst):
            return lst[index]

mean = random.randint(0,43)

for _ in range(20):
    print(normal_choice(lines, mean=mean, stddev=2))