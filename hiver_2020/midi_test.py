import random
from pyo import *

pm_list_devices()

s = Server(audio='jack')

# Open all MIDI output devices.
s.setMidiOutputDevice(99)

# Then boot the Server.
s.boot()

# close pedal
s.ctlout(64, 0)

note_vel_min = 20
note_vel_max = 40

# set random-ish pattern time
pat_time = XnoiseDur(dist=11, min=0.15, max=0.25)

time_counter = 0
def time_events():
    global time_counter, pat_time
    time_counter = time_counter + 1
    print(time_counter)
    print((pat_time.min, pat_time.max))
    
    # bass resonant slow
    # TODO: midi vel down
    if time_counter == 45:
        # sustain pedal on
        s.ctlout(64, 127)
        pat_time.dist = 0
        pat_time.max = 15
        pat_time.min = 8

    # 1:30
    # accelerando... higher range, pedal off
    if time_counter == 140:
        s.ctlout(64, 0)
        pat_time.dist = 11
        pat_time.max = 1
        pat_time.min = .5
  
    if time_counter > 140 and pat_time.min > .1:
        pat_time.max = pat_time.min
        pat_time.min = pat_time.min - .005

    if time_counter == 200:
        s.ctlout(64, 127)

    # desacelerando vers très aigu pedal on
    #s.ctlout(64, 127)

# Actual time counter
global_time = Pattern(time_events, 1).play()

# patterns:
# freq = 9, mul = 48, add = 36
pitch = Phasor(freq=1, mul=48, add=1)

# Global variable to count the down and up beats.
count = 0
mul_count = 0
freq_count = 0

# make a class
def midi_event():
    global count, mul_count, pitch, freq_count, s
    
    # Retrieve the value of the pitch audio stream and convert it to an int.
    pit = int(pitch.get())
   
    # each 23 seconds
    mul_count = mul_count + 1
    if mul_count == 23:
        #pitch.mul = pitch.mul + 1
        pitch.add = pitch.add + 1
        print("MUL: ", pitch.mul)
        mul_count = 0;

    # each 35 seconds
    freq_count = freq_count + 1
    if freq_count == 35:
        pitch.freq = random.randint(1,30)
        print("FREQ: ", pitch.freq)
        freq_count = 0;

    # If the count is 0 (down beat), play a louder and longer event, otherwise
    # play a softer and shorter one.
    if count == 0 and random.randint(0,1) < .5: # half chance
        vel = random.randint(40, 60)
        dur = random.randint(900,1000)
        #chord
        s.makenote(pitch=pit+12, velocity=vel, duration=dur, channel=1)
        s.makenote(pitch=pit+14, velocity=vel, duration=dur, channel=1)
        s.makenote(pitch=pit+16, velocity=vel, duration=dur, channel=1)
    else:
        vel = random.randint(50, 80)
        dur = random.randint(40, 50)
        s.makenote(pitch=pit, velocity=vel, duration=dur, channel=1)

    # Increase and wrap the count to generate a 4 beats sequence.
    count = (count + 1) % random.randint(12,13)


    print("pitch: %d, velocity: %d, duration: %d" % (pit, vel, dur))


# Generates a MIDI event every 125 milliseconds.
pat = Pattern(midi_event, pat_time).play()

s.gui(locals())
