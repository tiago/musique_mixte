from pyo import *
import random

s = Server(audio='jack').boot()
s.start()
a = OscReceive(port=10001, address=['/attention', # 0-100
                                    '/meditation', # 0-100
                                    '/rawValue', # ?
                                    '/delta', # 0.1Hz to 3Hz - Deep, dreamless sleep, non-REM sleep, unconscious
                                    '/theta', #
                                    '/lowAlpha', #
                                    '/highAlpha', #
                                    '/lowBeta', #
                                    '/highBeta', # 21Hz to 30H - Alertness, agitation
                                    '/lowGamma', #
                                    '/midGamma', #
                                    '/poorSignal', # 0-255 - how poor the signal measured is
                                    '/blinkStrength' # Doesn't work with serial communication
                                    ])

# Mappings
attention_to_freq = Scale(a['/attention'], 30, 100, 20, 800, exp=2)
attention_to_mul = Scale(a['/attention'], 30, 100, 0, 0.5)
meditation_to_freq = Scale(a['/meditation'], 30, 100, 20, 800, exp=2)
meditation_to_mul = Scale(a['/meditation'], 30, 100, 0, 0.5)
poorSignal_to_mul = Scale(a['/poorSignal'], 0, 255, 0, 0.5)

# Noise = poor signal
noise = BrownNoise(mul=poorSignal_to_mul)

# Meditation synth
#synth_meditation = SumOsc(freq=SigTo(meditation_to_freq, time=0.8), ratio=SigTo(meditation_to_freq*.0011, time=0.8), mul=0.2)
synth_meditation = SuperSaw(freq=SigTo(meditation_to_freq, time=0.8), mul=0.2)

# Attention synth
lfd = Sine(.3, mul=.2, add=.5)
#synth_attention = SuperSaw(freq=SigTo(attention_to_freq, time=0.8), detune=lfd, bal=0.7, mul=0.2)
synth_attention = SineLoop(freq=SigTo(attention_to_freq, time=0.8), mul=0.2)

# Final output mixter
mix = Mix([synth_attention, synth_meditation, noise], voices=3).out()

# Plotting
spec = Spectrum(mix, size=2048)
scope = Scope(mix)

s.gui(locals())
